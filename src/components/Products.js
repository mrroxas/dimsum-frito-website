import React, { Component } from 'react'
import { Grid, Row, Col, Glyphicon} from 'react-bootstrap'

class Products extends Component {
    
  render() {
    return (
        <Row className="App-products">
            <p className="App-title hidden-sm hidden-xs">Our Menu</p>
            <div id="myCarousel" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators hidden">
                    <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div className="carousel-inner">
                    <div className="item active">
                        <img src="http://i68.tinypic.com/2upcto8.png" />
                        <div class="carousel-caption">
                        <h3>SIOPAO</h3>
                        <p>Dimsum Frito Express</p>
                        </div>
                    </div>
                    <div className="item">
                        <img src="http://i64.tinypic.com/oshoj9.png" />
                        <div class="carousel-caption">
                        <h3>SIOMAI</h3>
                        <p>Dimsum Frito Express</p>
                        </div>
                    </div>
                    <div className="item">
                        <img src="http://i67.tinypic.com/mvivdh.png" />
                        <div class="carousel-caption">
                        <h3>CHICKEN</h3>
                        <p>Dimsum Frito Express</p>
                        </div>
                    </div>
                    <div className="item">
                        <img src="http://i67.tinypic.com/2uhpmd4.png" />
                        <div class="carousel-caption">
                        <h3>NOODLES</h3>
                        <p>Dimsum Frito Express</p>
                        </div>
                    </div>
                    <div className="item">
                        <img src="http://i63.tinypic.com/2dinm6u.png" />
                        <div class="carousel-caption">
                        <h3>CHAI RICE IN CUP</h3>
                        <p>Dimsum Frito Express</p>
                        </div>
                    </div>
                    <div className="item">
                        <img src="http://i66.tinypic.com/25fruxi.png" />
                        <div class="carousel-caption">
                        <h3>SPECIAL DIMSUMS</h3>
                        <p>Dimsum Frito Express</p>
                        </div>
                    </div>
                    <div className="item">
                        <img src="http://i68.tinypic.com/2dwdgmh.png" />
                        <div class="carousel-caption">
                        <h3>SPECIAL DIMSUMS</h3>
                        <p>Dimsum Frito Express</p>
                        </div>
                    </div>
                    <div className="item">
                        <img src="http://i68.tinypic.com/t6e7ix.png" />
                        <div class="carousel-caption">
                        <h3>SPECIAL DIMSUMS</h3>
                        <p>Dimsum Frito Express</p>
                        </div>
                    </div>
                    
                </div>
                <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span className="glyphicon glyphicon-chevron-left"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="right carousel-control" href="#myCarousel" data-slide="next">
                    <span className="glyphicon glyphicon-chevron-right"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
        </Row>
    );
  }
}

export default Products;

