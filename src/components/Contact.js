import React, { Component } from 'react'
import { Grid, Row, Col, Glyphicon} from 'react-bootstrap'

class Contact extends Component {
    
  render() {
    return (
        <Row className="App-meeting" id="App-meeting">
          <Col md={12}><p className="App-title">Schedule a Meeting.</p></Col>
          <Col sm={12} xs={12} className="hidden-md hidden-lg">
            <p className="info">Tell us your bright ideas and let's tranform them into a successful innovation.</p>
            <Col md={12}>
              <ul>
                <li><Glyphicon glyph="envelope" title="Email us at dimsumfrito@gmail.com" /> dimsumfrito@gmail.com</li>
                <li><Glyphicon glyph="phone" title="Call us at (02) 571-3629" /> (02) 571-3629</li>
              </ul>
            </Col>
            <hr />
          </Col>
          <Col md={12} className="hidden-sm hidden-xs">
            <p className="info">We hope to hear a word from you. A delicious Dimsum day to you!</p>
            <Col md={12} align='center'>
              <ul>
                <li><Glyphicon glyph="envelope" title="Email us at dimsumfrito@gmail.com" /> dimsumfrito@gmail.com</li>
                <li><Glyphicon glyph="phone" title="Call us at (02) 571-3629" /> (02) 571-3629</li>
              </ul>
            </Col>
          </Col>
        </Row>
    );
  }
}

export default Contact;

