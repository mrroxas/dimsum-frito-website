import React, { Component } from 'react'
import { Grid, Row, Col, Glyphicon} from 'react-bootstrap'

class Banner extends Component {
    
  render() {
    return (
        <Row className="App-banner">
          <Col md={12} className="hidden-sm hidden-xs">
            <img alt="Dimsum Frito Express" className="App-logo" src="http://i68.tinypic.com/2vtp6w5.png" title="Dimsum Frito Express" />
          </Col>
          <Col sm={12} xs={12} className="hidden-md hidden-lg">
          <img alt="Dimsum Frito Express" className="App-logo" src="http://i68.tinypic.com/2vtp6w5.png" title="Dimsum Frito Express" />
          </Col>
          <Col md={4} sm={12} xs={12} className="App-identity hidden">
          <p className="App-name"><strong>Dimsum Frito Express</strong></p>
          <p className="App-moto">And a catchy phrase down here.</p>
          </Col>
        </Row>
    );
  }
}

export default Banner;

