import React, { Component } from 'react'
import { Grid, Row, Col, Glyphicon} from 'react-bootstrap'

class About extends Component {
    
  render() {
    return (
        <Row className="App-walks">
          <Col md={12} sm={12} xs={12}>
            <p className="App-title">Dimsum Frito Express</p>
            <p className="App-writeups">
            In just a few months of operations at Metrowalk Bazaar, Dimsum Frito has attracted a number of franchisees. We acquired our first franchisee June 2005, then SM Pampanga on December 2005, SM North Edsa on June 2006, SM Mall of Asia on November 2006. 

            To date we have a total of 31 outlets, the newest SM San Fernando and SM Telabastagan.
            
            The goal of the company is to increase the number of Dimsum Frito outlets either thru company owned or thru partnership by franchising.


            </p>
            <div className="App-support">
              <img alt="Dimsum Frito Express" src="http://i63.tinypic.com/mhr2tw.png" />
              <a href="#App-meeting" className="btn btn-danger">Franchise Now</a>
            </div>
          </Col>
        </Row>
    );
  }
}

export default About;

