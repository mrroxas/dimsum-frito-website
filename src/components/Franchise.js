import React, { Component } from 'react'
import { Grid, Row, Col, Glyphicon} from 'react-bootstrap'

class About extends Component {
    
  render() {
    return (
        <Row className="App-franchise">
          <Col lg={12} md={12} sm={12} xs={12}>
            <p className="App-title">Franchise with Us</p>
          </Col>
          <Col lg={6} md={6} sm={12} xs={12} className='package' align='center'>
            <h1>DIMSUM FRITO FRANCHISE PACKAGE</h1>
            <p>CART WITH MENU AND SIGNAGE</p>
            <p>BASIC EQUIPMENT</p>
            <p>FREEZER</p>
            <p>SMALL WARES</p>
            <p>INITIAL STOCKS</p>
            <p>ORDERING/CASHIER’S SYSTEM-POS TYPE (OPTIONAL)</p>
          </Col>
          <Col lg={6} md={6} sm={6} xs={6} align='center hidden-sm hidden-xs'>
            <img alt="Dimsum Frito Express" src="http://i67.tinypic.com/aow5qq.png" />
          </Col>
        </Row>
    );
  }
}

export default About;

