import React, { Component } from 'react'
import { Grid, Row, Col, Glyphicon} from 'react-bootstrap'

class Contact extends Component {
    
  render() {
    return (
        <Row className="App-power">
          <Col md={12}>Copyright 2018. Dimsum Frito Express. Powered by <a href='http://circletechit.com/' target='_blank'>Circle Tech IT Solutions</a>.</Col>
        </Row>
    );
  }
}

export default Contact;

