import React, { Component } from 'react'
import axios from "axios"
import { Grid, Row, Col, Glyphicon} from 'react-bootstrap'
import './App.css'
import Banner from "./components/Banner"
import About from "./components/About"
import Franchise from "./components/Franchise"
import Products from "./components/Products"
import Contact from "./components/Contact"
import Powered from "./components/Powered"

class App extends Component {
  handleContactModalOpen(){
    this.setState({openContactModal: true})
  }
  handleContactModalClose(){
    this.setState({openContactModal: false, contactSuccess: false})
  }
  handleSubmit(e){
    e.preventDefault()
    const header = { 
      "async": true,
      "crossDomain": true,
      "method": "POST",
      "headers": { 
      "Content-Type": "application/x-www-form-urlencoded",
      "cache-control": "no-cache",
      }
    };
    const form = new FormData()
    form.append("name", this.refs.name.value)
    form.append("email", this.refs.email.value)
    form.append("subject", this.refs.subject.value)
    form.append("body", this.refs.body.value)
    // axios.post('http://waynepowertrading.com/mail/mailer.php', form, header);
    // this.setState({openContactModal: false, contactSuccess: true})
  }
  render() {
    return (
      <Grid className="App">
        <Banner />
        <About />
        <Franchise />
        <Products />
        <Contact />
        <Powered />
      </Grid>
    );
  }
}

export default App;
